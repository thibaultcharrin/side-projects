#!/bin/bash
function linuxb_tekton() {
    echo -e "\n${TIGER} Tekton CLI "
    VERSION=0.39.1 # CHANGE ME :)

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Tekton CLI "
        echo "https://tekton.dev/docs/cli/"
        echo "https://github.com/tektoncd/cli/releases"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            ${LINUXB_PKG_INSTALL} tektoncd-cli
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v tkn >/dev/null ||
            ! command -v kubectl-tkn >/dev/null ||
            [[ $(tkn version --component client) != "$VERSION" ]]; then
            # Before_Script
            # curl
            linuxb_os_check
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            curl -L -o ~/tkn.tar.gz "https://github.com/tektoncd/cli/releases/download/v${VERSION}/tkn_${VERSION}_Linux_x86_64.tar.gz"
            tar -xzvf ~/tkn.tar.gz
            chmod +x ~/tkn
            sudo mv -v ~/tkn /usr/local/bin

            if ! command -v kubectl-tkn; then
                sudo ln -s /usr/local/bin/tkn /usr/local/bin/kubectl-tkn
            fi

            rm -v ~/tkn.tar.gz
        else
            echo "Already present "
            tkn version --component client
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_tekton "$@"
