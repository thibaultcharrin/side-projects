#!/bin/bash
function linuxb_kubectl() {
    echo -e "\n${OCTOPUS} Kubernetes - kubectl "
    KUBECTL_VERSION=v1.31 # CHANGE ME :)

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Kubernetes - kubectl "
        echo "https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            ${LINUXB_PKG_INSTALL} kubectl
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v kubectl >/dev/null; then
            # Before_Script
            # curl
            linuxb_os_check
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
            sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
        else
            echo "Already present "
            kubectl version --client
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_kubectl "$@"
