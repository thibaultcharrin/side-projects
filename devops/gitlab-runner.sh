#!/bin/bash
function linuxb_gitlab_runner() {
    echo -e "\n${ROCKET} GitLab Runner "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install GitLab Runner "
        echo "https://docs.gitlab.com/runner/install/"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            # Download the binary for your system
            sudo curl --output /usr/local/bin/gitlab-runner "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-darwin-amd64"

            # Give it permission to execute
            sudo chmod +x /usr/local/bin/gitlab-runner

            # The rest of the commands execute as the user who will run the runner
            # Register the runner (steps below), then run
            cd ~
            gitlab-runner install
            gitlab-runner start
            return 0
        fi

        # LINUX
        if ! command -v gitlab-runner >/dev/null; then
            # Before_Script
            # curl
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            # Download the binary for your system
            sudo curl -L --output /usr/local/bin/gitlab-runner "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/binaries/gitlab-runner-linux-amd64"

            # Give it permission to execute
            sudo chmod +x /usr/local/bin/gitlab-runner

            # Create a GitLab Runner user
            sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

            # Install and run as a service
            sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
            sudo gitlab-runner start

        else
            echo "Already present "
            gitlab-runner --version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_gitlab_runner "$@"
