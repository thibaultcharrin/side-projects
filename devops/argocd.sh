#!/bin/bash
function linuxb_argocd() {
    echo -e "\n${OCTOPUS} ArgoCD "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install ArgoCD "
        echo "https://argo-cd.readthedocs.io/en/stable/cli_installation/"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            ${LINUXB_PKG_INSTALL} argocd
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v argocd >/dev/null; then
            # Before_Script
            # curl
            if ! command -v curl >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl
            fi
            # Script
            curl -L -o ~/argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
            sudo install -m 555 ~/argocd-linux-amd64 /usr/local/bin/argocd
            rm -v ~/argocd-linux-amd64
        else
            echo "Already present "
            argocd version --client
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_argocd "$@"
