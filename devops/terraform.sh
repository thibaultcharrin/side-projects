#!/bin/bash
function linuxb_terraform() {
    echo -e "\n${PENGUIN} Terraform "
    VERSION=1.10.5 # CHANGE ME :)

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Terraform "
        echo "https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            brew tap hashicorp/tap
            ${LINUXB_PKG_INSTALL} hashicorp/tap/terraform
            ${LINUXB_PKG_UPDATE}
            brew upgrade hashicorp/tap/terraform
            return 0
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v terraform >/dev/null; then
            # Before_script
            # curl
            linuxb_os_check
            if ! command -v curl >/dev/null ||
                ! command -v unzip >/dev/null; then
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} curl unzip
            fi
            # Script
            curl -L -o ~/terraform.zip "https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_linux_amd64.zip"
            unzip ~/terraform.zip
            chmod +x ~/terraform
            sudo mv -v ~/terraform /usr/local/bin/terraform

            rm -v ~/terraform.zip
        else
            echo "Already present "
            terraform version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_terraform "$@"
