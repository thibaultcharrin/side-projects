#!/bin/bash

# INJECTION CONTROL (via sed)
# injected_pkg_arrays
${TEMP_PKG_ARRAYS}

ALL_PKGS=(
	"${DEV[@]}"
	"${DEVOPS[@]}"
	"${SYSADMIN[@]}"
	"${ZSH[@]}"
)

# injected_functions
${TEMP_FUNCTIONS}

# CORE FUNCTIONS
function linuxb_all() {
	case $2 in
	"--help" | "help" | "-h" | "h")
		echo
		echo "Usage: linuxb all [OPTION]"
		echo
		echo "Install all packages"
		echo
		echo "Options:"
		echo "  -f, --force    force install"
		echo "  -y, --yes      install without confirmation"
		echo "  -h, --help     display this help and exit"
		return 0
		;;
	*)
		case $2 in
		"-y" | "--yes" | "YES" | "Yes" | "yes" | "y") commit="y" ;;
		"-f" | "--force" | "force" | "F" | "f")
			echo "[toggle: force-install all packages]"
			force="-f"
			;;
		esac

		if [[ $commit == "" ]]; then
			echo -e "\n${UNICORN} WARNING! You are about to install ALL available packages (Programming Languages, DevOps Tools, and SysAdmin Tools)."
			echo "This may take some time. Would you like to proceed?"
			read -rp "(y/n) ? " commit
		fi

		if [[ "${commit:=n}" != "y" ]]; then
			echo "no package was installed "
		else
			for PKG in "${DEV[@]}"; do
				"linuxb_$(echo "${PKG}" | cut -d "/" -f 2 | cut -d "." -f 1 | sed 's|-|_|g')" "$force"
			done
			for PKG in "${DEVOPS[@]}"; do
				"linuxb_$(echo "${PKG}" | cut -d "/" -f 2 | cut -d "." -f 1 | sed 's|-|_|g')" "$force"
			done
			for PKG in "${SYSADMIN[@]}"; do
				"linuxb_$(echo "${PKG}" | cut -d "/" -f 2 | cut -d "." -f 1 | sed 's|-|_|g')" "$force"
			done
			echo -e "\nYou are all set! ${UNICORN} "
			echo -e "\nIf Docker was installed, run the following command:\n\tnewgrp docker"
		fi
		;;
	esac
	return 0
}

function linuxb_install() {
	for PKG in "${ALL_PKGS[@]}"; do
		if [[ "$2" == $(echo "$PKG" | cut -d "/" -f 2 | cut -d "." -f 1) ]]; then
			"linuxb_${2//-/_}" "$3"
			return 0
		fi
	done
	echo
	echo "Usage: linuxb install [PACKAGE] [OPTION]"
	echo
	echo "Install specific package"
	echo
	echo "Options:"
	echo "  -f, --force    force install"
	echo "  -h, --help     get help from this package and exit"
	echo
	echo "Please run the script passing a supported package name as argument. For example:"
	echo -e "\nlinuxb install $(printf "%s\n" "${ALL_PKGS[@]}" | shuf -n1 | cut -d "/" -f 2 | cut -d "." -f 1)"
	return 0
}

function linuxb_list() {
	case $2 in
	"--help" | "help" | "-h" | "h")
		echo
		echo "Usage: linuxb list [OPTION]"
		echo
		echo "List available packages"
		echo
		echo "Options:"
		echo "  -h, --help    display this help and exit"
		;;
	*)
		echo -e "\nProgramming Languages:"
		for PKG in "${DEV[@]}"; do
			echo -e "$(echo "${PKG}" | cut -d "/" -f 2 | cut -d "." -f 1)"
		done
		echo -e "\nDevOps Tools:"
		for PKG in "${DEVOPS[@]}"; do
			echo -e "$(echo "${PKG}" | cut -d "/" -f 2 | cut -d "." -f 1)"
		done
		echo -e "\nSysAdmin Tools:"
		for PKG in "${SYSADMIN[@]}"; do
			echo -e "$(echo "${PKG}" | cut -d "/" -f 2 | cut -d "." -f 1)"
		done
		echo -e "\nZsh Tools:"
		for PKG in "${ZSH[@]}"; do
			echo -e "$(echo "${PKG}" | cut -d "/" -f 2 | cut -d "." -f 1)"
		done
		return 0
		;;
	esac
	return 0
}

function linuxb_upgrade() {
	case $2 in
	"--help" | "help" | "-h" | "h")
		echo
		echo "Usage: linuxb upgrade [OPTION]"
		echo
		echo "Upgrade packages, package manager and distribution"
		echo
		echo "Options:"
		echo "  -y, --yes      upgrade without confirmation"
		echo "  -h, --help     display this help and exit"
		return 0
		;;
	*)
		case $2 in
		"-y" | "--yes" | "YES" | "Yes" | "yes" | "y") commit="y" ;;
		esac

		if [[ $2 == "" ]]; then
			echo -e "\n${UNICORN} WARNING! You are about to upgrade ALL packages and this may cause your configuration to change. \
		\nWould you like to proceed? "
			read -rp "(y/n) ? " commit
		fi

		if [[ "${commit:=n}" != "y" ]]; then
			echo "no upgrade was performed "
		else
			linuxb_os_check
			${LINUXB_PKG_UPDATE}
			${LINUXB_PKG_UPGRADE}
			${LINUXB_PKG_AUTOREMOVE}
			${LINUXB_PKG_AUTOCLEAN}
			${LINUXB_PKG_CLEAN}
			echo -e "\nYou are all set! ${UNICORN}"
		fi
		;;
	esac
	return 0
}

function main() {
	case "$1" in
	"all" | "a") linuxb_all "$@" ;;
	"install" | "inst" | "ins" | "in" | "i") linuxb_install "$@" ;;
	"list" | "l" | "ls") linuxb_list "$@" ;;
	"upgrade" | "update" | "up" | "u") linuxb_upgrade "$@" ;;
	"wsl" | "w") linuxb_wsl_systemd "$@" ;;
	"-v" | "--version" | "Version" | "version" | "ver" | "v") linuxb_version ;;
	"-z" | "--zsh" | "Zsh" | "zsh" | "z") linuxb_zsh "$@" ;;
	*) linuxb_help ;;
	esac
	return 0
}

# ENTRYPOINT
linuxb_unicode_emojis
main "$@"
