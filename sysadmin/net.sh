#!/bin/bash
function linuxb_net() {
    echo -e "\n${PENGUIN} Networking Tools (traceroute mtr nmap iftop dnsutils/bind-utils) "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Networking Tools (traceroute mtr nmap iftop dnsutils/bind-utils) "
        echo "https://roadmap.sh/devops"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            echo "macOS is not yet supported"
            return 1
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v traceroute >/dev/null ||
            ! command -v mtr >/dev/null ||
            ! command -v nmap >/dev/null ||
            ! command -v iftop >/dev/null ||
            ! command -v nslookup >/dev/null; then
            case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr -d '"') in
            "ubuntu" | "debian")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} traceroute mtr nmap iftop dnsutils
                ;;
            "fedora" | "ol")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} traceroute mtr nmap iftop dnsutils
                ;;
            "arch")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} traceroute mtr nmap iftop dnsutils
                ;;
            "opensuse-tumbleweed")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} traceroute mtr nmap iftop bind-utils
                ;;
            *)
                echo "Generic installation not yet supported"
                return 1
                ;;
            esac
        else
            echo "Already present "
            traceroute -V
            mtr -v
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_net "$@"
