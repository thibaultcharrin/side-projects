#!/bin/bash
function linuxb_virt() {
    echo -e "\n${PENGUIN} Virtualization Tools (qemu-kvm libvirt virt-manager) "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Virtualization Tools (qemu-kvm libvirt virt-manager) "
        echo "https://minikube.sigs.k8s.io/docs/drivers/kvm2/"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            echo "macOS is not yet supported"
            return 1
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v virt-manager >/dev/null; then
            case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr -d '"') in
            "ubuntu")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils
                sudo adduser "$(id -un)" libvirt
                sudo adduser "$(id -un)" kvm
                ${LINUXB_PKG_INSTALL} virt-manager
                ;;
            "debian")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} qemu-system libvirt-daemon-system
                sudo adduser "$USER" libvirt
                virsh list --all
                virsh --connect qemu:///system list --all
                export LIBVIRT_DEFAULT_URI='qemu:///system'
                ${LINUXB_PKG_INSTALL} virt-manager
                virsh --connect=qemu:///system net-autostart default
                ${LINUXB_PKG_INSTALL} dnsmasq-base bridge-utils firewalld network-manager
                echo -e "[main]\ndns=dnsmasq" | sudo tee /etc/NetworkManager/conf.d/libvirt_dns.conf
                echo "server=/libvirt/192.168.122.1" | sudo tee /etc/NetworkManager/dnsmasq.d/libvirt_dns.conf
                echo
                echo "To finalize installation:"
                echo "1. run the command:"
                echo "virsh --connect=qemu:///system net-edit default"
                echo "2. add the following line (after the mac tag):"
                echo "<domain name='libvirt' localOnly='yes'/>"
                echo "3. run the command:"
                echo "sudo systemctl restart NetworkManager"
                ;;
            "ol")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} qemu-kvm libvirt libguestfs-tools virt-install virt-manager
                sudo systemctl enable --now libvirtd.service
                sudo usermod -aG libvirt $USER
                ;;
            "fedora")
                sudo dnf group install --with-optional virtualization
                sudo systemctl enable --now libvirtd
                sudo usermod -aG libvirt $USER
                ;;
            "arch")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} qemu-full qemu-img libvirt virt-install virt-manager virt-viewer edk2-ovmf dnsmasq swtpm guestfs-tools libosinfo tuned
                for drv in qemu interface network nodedev nwfilter secret storage; do
                    sudo systemctl enable virt${drv}d.service
                    sudo systemctl enable virt${drv}d{,-ro,-admin}.socket
                done
                sudo usermod -aG libvirt $USER
                if command -v bash >/dev/null; then
                    grep 'export LIBVIRT_DEFAULT_URI="qemu:///system"' ~/.bashrc || echo 'export LIBVIRT_DEFAULT_URI="qemu:///system"' >>~/.bashrc
                fi
                if command -v zsh >/dev/null; then
                    grep 'export LIBVIRT_DEFAULT_URI="qemu:///system"' ~/.zshrc || echo 'export LIBVIRT_DEFAULT_URI="qemu:///system"' >>~/.zshrc
                fi
                ;;
            "opensuse-tumbleweed")
                ${LINUXB_PKG_INSTALL} -t pattern kvm_server kvm_tools
                ;;
            *)
                echo "Generic installation not yet supported"
                return 1
                ;;
            esac
        else
            echo "Already present "
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_virt "$@"
