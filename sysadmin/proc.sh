#!/bin/bash
function linuxb_proc() {
    echo -e "\n${PENGUIN} Process Monitoring Tools (htop atop btop lsof iotop-c) "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Process Monitoring Tools (htop atop btop lsof iotop-c) "
        echo "https://roadmap.sh/devops"
        ;;
    *)
        linuxb_os_check

        # MACOS
        if [[ ${LINUXB_OS} == "Darwin" ]]; then
            echo "macOS is not yet supported"
            return 1
        fi

        # LINUX
        if [[ $1 == "-f" ]] || [[ $1 == "--force" ]] ||
            ! command -v htop >/dev/null ||
            ! command -v atop >/dev/null ||
            ! command -v btop >/dev/null ||
            ! command -v lsof >/dev/null; then
            case $(grep "^ID=" /etc/os-release | cut -d "=" -f 2 | tr -d '"') in
            "ubuntu" | "debian")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} htop atop btop lsof iotop-c
                ;;
            "fedora" | "ol")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} htop atop btop lsof iotop-c
                ;;
            "arch")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} htop atop btop lsof iotop-c
                ;;
            "opensuse-tumbleweed")
                ${LINUXB_PKG_UPDATE}
                ${LINUXB_PKG_INSTALL} htop atop btop lsof iotop-c
                ;;
            *)
                echo "Generic installation not yet supported"
                return 1
                ;;
            esac
        else
            echo "Already present "
            htop --version
            btop --version
        fi
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_proc "$@"
