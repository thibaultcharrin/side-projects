## 1.10.0 (2025-02-24)

### changed (2 changes)

- [Merge branch '15-update-documentation' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/d24ebb8111d25a41e5d729e03575aaab75b22978) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/24))
- [Merge branch '25-add-support-for-virt' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/31175dea8d9738956d19e4a826838aabcb947458) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/23))

## 1.9.0 (2025-02-21)

### changed (1 change)

- [Merge branch '19-add-docker-generic-installation' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/6b4a4507ae25836134d2513c257301b1c63adefe) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/22))

## 1.8.0 (2025-02-20)

### changed (4 changes)

- [Merge branch '24-stream-command-v-to-dev-null' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/d2aedd2790e0416c865dc4c3453e1c83d04be06c) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/21))
- [Merge branch '22-node-check-not-working' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/a3b98c0e36d26226895b3bcf4f9b7257f3aae21d) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/20))
- [Merge branch '21-fix-curl-6-could-not-resolve-host-git' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/d44b17741033be1293b7297d5f557219ddfab277) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/19))
- [Merge branch '20-add-further-support-for-sysadmin-packages' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/d47d8150c4b5ae1afeed3ac2f6e1c255abb8c640) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/18))

## 1.7.0 (2025-02-19)

### changed (3 changes)

- [Merge branch '16-implement-agnostic-kubectl-install' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/b1697759309b8bca8d47875a410cde0fdccec405) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/17))
- [Merge branch '17-implement-agnostic-tekton-cli-install' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/5453455469b68bf87b18afbbc3fc1772c93cc1b6) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/16))
- [Merge branch '18-implement-agnostic-terraform-install' into 'main'](https://gitlab.com/devops-collab/linuxb/-/commit/934c6cd140819361ecedcdf6ea30256878174319) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/15))

## 1.6.2 (2024-11-22)

### fixed (1 change)

- [Fix knative install version check](https://gitlab.com/devops-collab/linuxb/-/commit/ece2a83c6fe524e91e3604dee53796311d96f3dd) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/14))

## 1.6.1 (2024-11-19)

### changed (1 change)

- [Review installation methods](https://gitlab.com/devops-collab/linuxb/-/commit/3aac2322ad4ff572db33674e63b1feada43b4fdc) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/13))

## 1.6.0 (2024-08-22)

### added (1 change)

- [add terraform](https://gitlab.com/devops-collab/linuxb/-/commit/14e16a4af87acf687cd3fbac4565915d1d60fb90) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/12))

## 1.5.0 (2024-07-21)

### changed (1 change)

- [Update debian based installations and added virtualization for Debian](https://gitlab.com/devops-collab/linuxb/-/commit/2112b03715cb9b23bc3a1815eb7e65f57b853cbe) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/11))

## 1.4.3 (2024-06-19)

### added (1 change)

- [Add Qemu / KVM2 support](https://gitlab.com/devops-collab/linuxb/-/commit/199aa4c586d0274e130ab5ccf911609602d4f8fe) ([merge request](https://gitlab.com/devops-collab/linuxb/-/merge_requests/10))

## 1.4.2 (2024-04-16)

### changed (1 change)

- [Rework essentials](devops-collab/linuxb@355e64ae5ae14d203facac89649fea6c837e2f8b) ([merge request](devops-collab/linuxb!8))

## 1.4.1 (2024-02-05)

### changed (1 change)

- [Merge branch '1-minikube-binary-not-removed-after-installed-check-platform-arch' into 'main'](devops-collab/linuxb@66ddcd1ec7a81972477954433d16b4b567afc187) ([merge request](devops-collab/linuxb!7))

## 1.4.0

### changed (3 changes)

- Simplified linuxb install/uninstall
- DOC: patch WSL now part of linuxb core functionalities
- updated CI

## 1.3.0

### changed (2 changes)

- wsl-systemd patch now part of CLI
- Addressed Debian bookworm WSLInterop issue

## 1.2.0

### changed (3 changes)

- better shortcuts
- improved helpers
- shortened CI/CD

## 1.1.0

### changed (2 changes)

- improved helpers with finer granularity
- streamlined CLI with "all", "install", "update" commands

## 1.0.0

### changed (2 changes)

- linuxb cli (copy to /usr/local/bin)
- distribution agnostic installations
