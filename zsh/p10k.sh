#!/bin/bash
function linuxb_p10k() {
    echo -e "\n${UNICORN} Zsh - p10k theme "

    case $1 in
    "--help" | "help" | "-h" | "h")
        echo "Install Zsh - p10k theme "
        echo "https://github.com/romkatv/powerlevel10k#installation"
        ;;
    *)
        linuxb_os_check

        # Before_Script
        # zsh
        if ! command -v zsh >/dev/null ||
            [[ ! -d ~/.oh-my-zsh/ ]]; then
            linuxb_zsh
        fi
        # curl, git
        if ! command -v curl >/dev/null ||
            ! command -v git >/dev/null; then
            ${LINUXB_PKG_UPDATE}
            ${LINUXB_PKG_INSTALL} curl git
        fi

        # Script
        echo -e "\nChecking Fonts "
        if [[ ! -d ~/fonts/ ]]; then
            echo -e "Would you like to download fonts?"
            read -rp "(y/n)?" COMMIT
            if [[ ${COMMIT} == "y" ]]; then
                mkdir ~/fonts/
                curl -Lo ~/fonts/meslolgs-nf-regular.ttf "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf"
            else echo "no font was downloaded"; fi
        else echo "Already present "; fi

        echo -e "\nChecking Powerlevel10k Theme "
        CURRENT_THEME=$(grep ZSH_THEME= ~/.zshrc | head -n 1 | cut -d "=" -f2)
        THEME="powerlevel10k/powerlevel10k"

        if [[ "${CURRENT_THEME}" != "${THEME}" ]]; then
            git clone --depth=1 https://github.com/romkatv/powerlevel10k.git "${ZSH_CUSTOM:-${HOME}/.oh-my-zsh/custom}"/themes/powerlevel10k
            sed -i "s|ZSH_THEME=${CURRENT_THEME}|ZSH_THEME=${THEME}|g" ~/.zshrc
        else
            echo "Already present "
        fi

        echo
        echo "Final Steps "
        echo "Please install the fonts to your terminal "
        echo "for example (on WSL), run the command: "
        echo -e "\texplorer.exe . "
        echo
        echo "Copy \$HOME/fonts/ folder to your Windows desktop, "
        echo "open/install each *.ttf file and set MesloLGS NF as the default font in Windows Terminal"
        echo
        echo "Finally, run the command: "
        echo -e "\t/bin/zsh"
        echo
        echo "You may change the configuration of the theme at any time with the command: "
        echo -e "\tp10k configure"
        return 0
        ;;
    esac
}
#REGION_REMOVED_BY_BUILD_SH
# shellcheck source=/dev/null
. ../core/unicode-emojis.sh
# shellcheck source=/dev/null
. ../core/os-check.sh
linuxb_p10k "$@"
